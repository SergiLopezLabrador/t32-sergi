package com.t32.service;

import java.util.List;

import com.t32.dto.products;

public interface productsService {
	
	public List<products> listarproducts();
	
	public products guardarproducts(products products);
	
	public products productsXID(int id);
	
	public products actualizarproducts(products products);
	
	public void eliminarproducts(int id);

}
