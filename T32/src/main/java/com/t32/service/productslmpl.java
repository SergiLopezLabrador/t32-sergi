package com.t32.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t32.dto.products;
import com.t32.dao.productsDao;


@Service
public class productslmpl implements productsService {
	
	@Autowired
	productsDao productsDao;

	@Override
	public List<products> listarproducts() {
		return productsDao.findAll();
	}

	@Override
	public products guardarproducts(products products) {
		return productsDao.save(products);
	}

	@Override
	public products productsXID(int id) {
		return productsDao.findById(id).get();
	}

	@Override
	public products actualizarproducts(products products) {
		return productsDao.save(products);
	}

	@Override
	public void eliminarproducts(int id) {
		productsDao.deleteById(id);
	}


}
