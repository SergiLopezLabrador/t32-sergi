package com.t32.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.t32.dto.products;
import com.t32.service.productslmpl;


@RestController
@RequestMapping("/api")
public class productsController {

	@Autowired
	productslmpl productslmpl;
	
	@GetMapping("/products")
	public List<products> listarproducts(){
		return productslmpl.listarproducts();
	}
	
	@PostMapping("/products")
	public products salvarproducts(@RequestBody products products) {
		
		return productslmpl.guardarproducts(products);
	}
	
	@GetMapping("/products/{id}")
	public products productsXID(@PathVariable(name="id") int id) {
		
		products products_xid= new products();
		
		products_xid=productslmpl.productsXID(id);
		
		System.out.println("products XID: "+products_xid);
		
		return products_xid;
	}

	@PutMapping("/products/{id}")
	public products actualizarproducts(@PathVariable(name="id")int id,@RequestBody products products) {
		
		products products_seleccionado= new products();
		products products_actualizado= new products();
		
		products_seleccionado= productslmpl.productsXID(id);
		
		products_seleccionado.setName(products.getName());
		products_seleccionado.setDetail(products.getDetail());
		products_seleccionado.setCreated_at(products.getCreated_at());
		products_seleccionado.setUpdated_at(products.getUpdated_at());
		
		products_actualizado = productslmpl.actualizarproducts(products_seleccionado);
		
		System.out.println("El cajero actualizado es: "+ products_actualizado);
		
		return products_actualizado;
	}

	@DeleteMapping("/products/{id}")
	public void eliminarproducts(@PathVariable(name="id")int id) {
		productslmpl.eliminarproducts(id);
	}
	
}
