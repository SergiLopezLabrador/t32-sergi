package com.t32.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.t32.dto.products;

public interface productsDao extends JpaRepository<products, Integer> {

}
